# How to set up a development environment for Dedicated on an M1 Mac in a container

## Install dependencies

This guide uses [podman](https://podman.io/). You can use another container engine but the instructions here will differ.

1. Install `podman`:

    ```
    brew install podman
    ```
    

1. Create a VM and start it:

    The `-v $HOME:$HOME` means you want to mount your host home directory at an identical path in the VM.

    ```
    podman machine init --cpus=4 --disk-size=60 --memory=8192 -v $HOME:$HOME
    podman machine start
    ```

1. SSH into the VM and install `qemu-user-static` so you can run `amd64` images on your M1 Mac, which has `aarch64`/`arm64` architecture.

    ```
    podman machine ssh
    sudo rpm-ostree install qemu-user-static
    exit
    ```

1. Optionally, alias `podman` as `docker` in your shell config file such as `~/.zshrc` or `~/.bashrc`:

    ```
    alias docker="podman"
    ```

## Build a dedicated-dev-environment image

1. Clone this repo:

    ```
    git clone git@gitlab.com:mkozono/dedicated-dev-environment.git
    ```

1. If you don't already have one, [create an SSH key and associate it with your GitLab account](https://docs.gitlab.com/ee/user/ssh.html). In this example, we place it at `~/.ssh/id_ed25519_dedicated_dev_env`. For now, the SSH key must have no passphrase since you cannot get an interactive shell with a Docker build.
<!-- This step does not work for now since the `--ssh` `podman build` option is broken in the Buildah version included with Podman 4.1.1.
1. Add your SSH key to SSH agent: `ssh-add ~/.ssh/id_ed25519_dedicated_dev_env`. This is used to forward the usage of your SSH key to the build container so it can clone private Dedicated repos, for example. See [this article for more information](https://medium.com/@tonistiigi/build-secrets-and-ssh-forwarding-in-docker-18-09-ae8161d066).
-->
1. Build the image

    ```
    cd dedicated-dev-environment
    podman build --format docker --secret id=id_ed25519,src=$HOME/.ssh/id_ed25519_dedicated_dev_env -t registry.gitlab.com/mkozono/dedicated-dev-environment .
    ```

1. Push the image

    ```
    podman push registry.gitlab.com/mkozono/dedicated-dev-environment
    ```

## Run the container

1. Run:

    `$HOME` refers to the home directory of the podman VM, `/root`. So if you initialized the VM with `-v $HOME:$HOME`, and the host directory you want to mount is `~/Developer/dedicated`, then use:

    ```
    podman run -it --name ded-dev-env -v "$HOME/Developer/dedicated:/dedicated" registry.gitlab.com/mkozono/dedicated-dev-environment /bin/bash
    ```

1. TODO: Make it easier to use your host SSH keys in the container. It might be possible to forward your SSH agent to the container with Podman etc. For now it is at least possible to copy your host's `~/.ssh/id_ed25519_dedicated_dev_env` into the `/dedicated` directory to pass it into the container, and from there copy it to the container's `~/.ssh/id_ed25519`.

1. Set your Git email and name:

    ```
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    ```

1. Do work! Note that `/build_dedicated` was only used to build the container. You probably want to work in `/dedicated` (or wherever you mounted your host volume).

1. If you exit it, you can start it and log in again with:

    ```
    podman start ded-dev-env
    podman exec -it ded-dev-env /bin/bash
    ```

## Troubleshooting

### Error during `podman machine start`

```
podman machine start
Starting machine "podman-machine-default"
Waiting for VM ...
Mounting volume... /Users/gahealy:/Users/gahealy
Error: exit status 255
```

I restarted my Mac and did not encounter it again. See more details in https://github.com/containers/podman/issues/14237.

### Error `qmp_podman-machine-default.sock: No such file or directory`

I removed the VM with `podman machine rm` and started over.

Related to https://github.com/containers/podman/issues/10824 though it did not solve the problem for me.

### Error starting an existing, previously-working container

In the below case, the VM somehow lost the mounted host directory:

```
➜  dedicated-dev-environment git:(main) docker ps -a
CONTAINER ID  IMAGE                                                         COMMAND     CREATED     STATUS                   PORTS       NAMES
1e068bd06431  registry.gitlab.com/mkozono/dedicated-dev-environment:latest  /bin/bash   3 days ago  Exited (127) 3 days ago              ded-dev-env
➜  dedicated-dev-environment git:(main) docker start ded-dev-env
Error: unable to start container "1e068bd06431efe91928d73d176141bc3584216b8260e217bcf6094785665f3b": crun: error stat'ing file `/Users/mkozonogitlab/Developer/dedicated`: No such file or directory: OCI runtime attempted to invoke a command that was not found
➜  dedicated-dev-environment git:(main) docker rm ded-dev-env
1e068bd06431efe91928d73d176141bc3584216b8260e217bcf6094785665f3b
➜  dedicated-dev-environment git:(main) podman run -it --name ded-dev-env -v "$HOME/Developer/dedicated:/dedicated" registry.gitlab.com/mkozono/dedicated-dev-environment /bin/bash
Error: statfs /Users/mkozonogitlab/Developer/dedicated: no such file or directory
➜  dedicated-dev-environment git:(main) podman machine ssh
Connecting to vm podman-machine-default. To close connection, use `~.` or `exit`
Warning: Permanently added '[localhost]:58610' (ED25519) to the list of known hosts.
Fedora CoreOS 36.20220806.2.0
Tracker: https://github.com/coreos/fedora-coreos-tracker
Discuss: https://discussion.fedoraproject.org/tag/coreos

Last login: Thu Jul 28 11:55:23 2022 from 192.168.127.1
[core@localhost ~]$ ls /Users
ls: cannot access '/Users': No such file or directory
[core@localhost ~]$ exit
logout
Connection to localhost closed.
Error: exit status 2
```

A workaround is to restart the VM:

```
➜  dedicated-dev-environment git:(main) podman machine stop
Machine "podman-machine-default" stopped successfully
➜  dedicated-dev-environment git:(main) podman machine start
Starting machine "podman-machine-default"
Waiting for VM ...
Mounting volume... /Users/mkozonogitlab:/Users/mkozonogitlab

This machine is currently configured in rootless mode. If your containers
require root permissions (e.g. ports < 1024), or if you run into compatibility
issues with non-podman clients, you can switch using the following command:

	podman machine set --rootful

API forwarding listening on: /var/run/docker.sock
Docker API clients default to this address. You do not need to set DOCKER_HOST.

Machine "podman-machine-default" started successfully
➜  dedicated-dev-environment git:(main) podman machine ssh
Connecting to vm podman-machine-default. To close connection, use `~.` or `exit`
Warning: Permanently added '[localhost]:58610' (ED25519) to the list of known hosts.
Fedora CoreOS 36.20220806.2.0
Tracker: https://github.com/coreos/fedora-coreos-tracker
Discuss: https://discussion.fedoraproject.org/tag/coreos

Last login: Thu Aug 11 18:20:25 2022 from 192.168.127.1
[core@localhost ~]$ ls /Users/mkozonogitlab
Applications  Desktop  Developer  Documents  Downloads  Library  Movies  Music  Pictures  Public  gitlab_logo.png
➜  dedicated-dev-environment git:(main) podman run -it --name ded-dev-env -v "$HOME/Developer/dedicated:/dedicated" registry.gitlab.com/mkozono/dedicated-dev-environment /bin/bash
root@d03adbecd81d:/build_dedicated#
```