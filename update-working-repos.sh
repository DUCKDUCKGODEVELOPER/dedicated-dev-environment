#!/bin/bash

cd amp && git pull && \
cd ../tenctl && git pull && \
cd ../asdf-tenctl && git pull && \
cd ../instrumentor && git pull && \
cd ../dedicated-container-image && git pull && \
cd ../team && git pull && \
cd ../switchboard && git pull && \
cd .. && \

cd amp && ./scripts/prepare-dev-env.sh && \
cd ../tenctl && ./scripts/prepare-dev-env.sh && \
cd ../asdf-tenctl && ./scripts/prepare-dev-env.sh && \
cd ../instrumentor && ./scripts/prepare-dev-env.sh && \
cd ../dedicated-container-image && ./scripts/prepare-dev-env.sh && \
cd ../team && ./scripts/prepare-dev-env.sh && \
cd ../switchboard && \ # ./scripts/prepare-dev-env.sh && \
cd ..