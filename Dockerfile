# syntax=docker/dockerfile:1
FROM --platform=linux/amd64 debian:stable
VOLUME /dedicated
WORKDIR /build_dedicated
SHELL ["/bin/bash", "-c"]
RUN apt update
RUN apt install -y \
    make build-essential libssl-dev zlib1g-dev \
    libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
    libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev \
    git vim unzip python3 python3-pip dialog chrony

# Install asdf
RUN git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.0 && \
    echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc && \
    echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc

# Install golang globally
RUN . ~/.bashrc && \    
    asdf plugin add golang && \
    asdf install golang 1.17.11 && \
    asdf global golang 1.17.11
    
# Install python globally
RUN . ~/.bashrc && \
    asdf plugin add python && \
    asdf install python 3.9.12 && \
    asdf global python 3.9.12

# https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/-/blob/main/DEVELOPMENT.md#developer-documentation
# suggests to use direnv and add a convenience function to log in to 1password
RUN . ~/.bashrc && \
    asdf plugin add direnv && asdf install direnv 2.32.1 && asdf global direnv 2.32.1 && \
    echo 'eval "$(direnv hook bash)"' >> ~/.bashrc && \
    echo 'login1p() { eval "$(op signin gitlab)"; direnv reload; }' >> ~/.bashrc && \
    cat ~/.bashrc

# Allow gitlab.com's SSH host's public key
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

# Clone Dedicated projects
RUN --mount=type=secret,id=id_ed25519,target=/root/.ssh/id_ed25519 . ~/.bashrc && \
    git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-dedicated/amp.git && \
    git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-dedicated/asdf-tenctl.git && \
    git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-dedicated/dedicated-container-image.git && \
    git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-dedicated/instrumentor.git && \
    git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-dedicated/switchboard.git && \
    git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-dedicated/team.git && \
    git clone git@gitlab.com:gitlab-com/gl-infra/gitlab-dedicated/tenctl.git && \
    git clone git@gitlab.com:mkozono/dedicated-dev-environment.git

# Workaround asdf not using global python when system python is specified
#
# Error:
#
# running pre-commit install...
# pre-commit installed at .git/hooks/pre-commit
# [INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
# [INFO] Initializing environment for https://github.com/scop/pre-commit-shfmt.
# [INFO] Initializing environment for https://github.com/scop/pre-commit-shfmt:mvdan.cc/sh/v3/cmd/shfmt@v3.5.1.
# [INFO] Initializing environment for https://github.com/golangci/golangci-lint.
# [INFO] Initializing environment for https://github.com/dnephin/pre-commit-golang.
# [INFO] Initializing environment for https://github.com/antonbabenko/pre-commit-terraform.
# [INFO] Initializing environment for https://github.com/google/go-jsonnet.
# [INFO] Initializing environment for https://github.com/python-jsonschema/check-jsonschema.
# [INFO] Initializing environment for https://github.com/bridgecrewio/checkov.git.
# [INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
# [INFO] Once installed this environment will be reused.
# [INFO] This may take a few minutes...
# An unexpected error has occurred: CalledProcessError: command: ('/root/.asdf/shims/python3', '/root/.cache/pre-commit-zipapp/AfsjsQP-AWXCEyHwt1AtiovjelxJTNDgEyg__hApLbc/python', '-mvirtualenv', '/root/.cache/pre-commit/repo0bt2a73v/py_env-python3.9')
# return code: 126
# expected return code: 0
# stdout: (none)
# stderr:
#     No version is set for command python3
#     Consider adding one of the following versions in your config file at
#     python 3.9.12
#
# Check the log at /root/.cache/pre-commit/pre-commit.log
#
# <And during another prepare script>
#
# running pre-commit install...
# pre-commit installed at .git/hooks/pre-commit
# [INFO] Initializing environment for https://github.com/adrienverge/yamllint.git.
# [INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
# [INFO] Once installed this environment will be reused.
# [INFO] This may take a few minutes...
# An unexpected error has occurred: CalledProcessError: command: ('/root/.asdf/shims/python3', '/root/.cache/pre-commit-zipapp/AfsjsQP-AWXCEyHwt1AtiovjelxJTNDgEyg__hApLbc/python', '-mvirtualenv', '/root/.cache/pre-commit/repo0bt2a73v/py_env-python3.9')
# return code: 126
# expected return code: 0
# stdout: (none)
# stderr:
#     No version is set for command python3
#     Consider adding one of the following versions in your config file at
#     python 3.9.12
#
# Check the log at /root/.cache/pre-commit/pre-commit.log
#
# This does not resolve the problem:
#
# asdf plugin add python && asdf install python 3.9.12 && asdf global python 3.9.12
#
# This does seem to resolve the problem:
RUN . ~/.bashrc && \
    cd amp && asdf local python 3.9.12 && \
    cd ../tenctl && asdf local python 3.9.12 && \
    cd ../asdf-tenctl && asdf local python 3.9.12 && \
    cd ../instrumentor && asdf local python 3.9.12 && \
    cd ../dedicated-container-image && asdf local python 3.9.12 && \
    cd ../team && asdf local python 3.9.12

# Install tons of stuff
RUN --mount=type=ssh . ~/.bashrc && \
    for i in */scripts/prepare-dev-env.sh; do echo "$i"; $i; done

CMD ["/bin/bash"]